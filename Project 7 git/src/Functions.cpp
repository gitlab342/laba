#include <fstream>
#include "Functions.hpp"
#include <climits>

namespace mt {
	void Read(int a[100][100]) {
		std::ifstream in("input.txt");
		int n, m;
		in >> n >> m;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				in >> a[i][j];
	}


	void Write(int& n, int& m,int a[100][100]) {
		std::ofstream out("output.txt");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++)
				out << a[i][j]<<' ';
			out << std::endl;
		}
	}


	int maxZ(int& n, int& m, int a[100][100]) {
		int max = INT_MIN;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (a[i][j] > max)
					max = a[i][j];
		int maxZ = 0;
		while (max > 0) {
			maxZ = maxZ + max % 10;
			max = max / 10;
		}
		return maxZ;
	}

	int minZ(int& n, int& m, int a[100][100]) {
		int min = INT_MAX;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (a[i][j] < min)
					min = a[i][j];
		int minZ = 0;
		while (min > 0) {
			minZ = minZ + min % 10;
			min = min / 10;
		}
		return minZ;
	}

	bool dif(int& n, int& m, int a[100][100]) {
		return (maxZ(n, m, a) - minZ(n, m, a)) <= 2;
	}


	void swap(int& n, int& m, int a[100][100]){
		int x, y;
		for (int k = 0; k < (n - 1); k++) {
			for (int i = k+1; i < n; i++) {
				x = 0; y = 0;
				for (int j = 0; j < m; j++) {
					x = x + a[i][j];

					y = y + a[k][j];
				}
				if (y > x) {
					for (int j = 0; j < m; j++) {
						std::swap(a[i][j], a[k][j]);
					}
				}

			}
		}

	}
}