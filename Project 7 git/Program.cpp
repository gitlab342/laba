#include <iostream>
#include <fstream>
#include "Functions.hpp"

using namespace std;
int main() {
	ifstream in("input.txt");
	ofstream out("output.txt");
	int A[100][100];
	int n, m;
	in >> n >> m;
	mt::Read(A);
	if (mt::dif(n, m, A))
		mt::swap(n, m, A);
	mt::Write(n,m,A);

}